# Peredelano demonstration

* Проверяем домен и наличие DNS записей
* Создем namespace:
    ```bash
    kubectl create ns peredelano
    ```
* Создаем issuer для cert manager, потому что мы любим когда у публичных сервисов есть сертификаты
    ```
    kubectl apply -f cert-manager.yaml
    ```
* Утсанавливаем helm если у вас его нет, инстукцию можно гялнуть [тут](https://helm.sh/docs/intro/install/). Для меня команда установки будет выглядеть так:
    ```bash
    sudo pacman -Syy helm
    ```
* Деплоим базу данных:
    ```bash
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm install coder-db bitnami/postgresql \
        --namespace peredelano \
        --set auth.username=coder \
        --set auth.password=coder \
        --set auth.database=coder \
        --set persistence.size=10Gi
    ```
* URL базы данных - `postgres://coder:coder@coder-db-postgresql.peredelano.svc.cluster.local:5432/coder?sslmode=disable`
* создаем секрет чтобы coder подключался к базе данных
  ```bash
  kubectl create secret generic coder-db-url -n peredelano \
   --from-literal=url="postgres://coder:coder@coder-db-postgresql.peredelano.svc.cluster.local:5432/coder?sslmode=disable"
  ```
* Создаем values файл и заполняем параметры
* Устанавливаем coder
    ```
    helm repo add coder-v2 https://helm.coder.com/v2    
    helm install coder coder-v2/coder \
        --namespace peredelano \
        --values values.yaml
    ```
* Наша платформа теперь доступна по адресу: [coder.mlops.fun](https://coder.mlops.fun)
